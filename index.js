import React, { PropTypes } from "react"

import dependencies from "./dependencies"

let {Layout} = dependencies

const PageNotFound = React.createClass({
  mixins: [Layout],
  render() {
    return (
      <div {...this.fill} className={"pagenotfound page " + this.props.position}>
        Page not founds
      </div>
    )
  }
})

export default PageNotFound
